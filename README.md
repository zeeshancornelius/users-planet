This project was created as an assignment for a company during its hiring process and bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on what the application is about and technologies used to power it up.<br>

## Table of Contents

- [About the App](#About)
- [Installation](#Installation)
- [Technologies](#Technologies)
- [Available Scripts](#Available-scripts)
  - [npm start](#npm-start)
  - [npm test](#npm-test)
  - [npm run build](#npm-run-build)
  - [npm run eject](#npm-run-eject)
- [Sending Feedback](#Feedback)  
- [Something Missing?](#Something-missing)

## About

The application takes care of the following tasks

* Fetch user's details from server and displays in a filter-enabled form.
* Displays a user's homeland details upon clicking in pop-up.

Please know this is my first application in react and redux for the sake of interview.

So feel free to contribute to make better.

## Installation

Clone the directory, then navigate to the downloaded directory on your local system and in terminal run the following command

### `npm install`

Then to run, type the following in terminal

### `npm start`

## Folder Structure

After creation, your project should look like this:

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
    manifest.json
  src/
    actions
      planet.js
      users.js
    reducers
      index.js
      planet.js
      users.js
    App.css
    App.js
    App.test.js
    index.css
    index.js
    store.js
    UserList.js
    UserPage.js
```

For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.

You can delete or rename the other files.

You may create subdirectories inside `src`, like place `UserList.js` and `UsersPage.js` in another directory named components. For faster rebuilds, only files inside `src` are processed by Webpack.<br>
You need to **put any JS and CSS files inside `src`**, otherwise Webpack won’t see them.

Only files inside `public` can be used from `public/index.html`.<br>

You can, however, create more top-level directories.<br>
They will not be included in the production build so you can use them for things like documentation.

## Feedback

Contact admin.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Technologies

Following is the list technologies used to power this app!

* [ES6](https://github.com/lukehoban/es6features)
* [REACT](https://facebook.github.io/react/)
* [REDUX](http://redux.js.org/)
* [REACT-ROUTER](https://github.com/ReactTraining/react-router)
* [SEMANTIC UI](https://react.semantic-ui.com/introduction)

## Displaying Lint Output in the Editor

>Note: this feature is available with `react-scripts@0.2.0` and higher.<br>
>It also only works with npm 3 or higher.

Some editors, including Sublime Text, Atom, and Visual Studio Code, provide plugins for ESLint.

They are not required for linting. You should see the linter output right in your terminal as well as the browser console. However, if you prefer the lint results to appear right in your editor, there are some extra steps you can do.

You would need to install an ESLint plugin for your editor first. Then, add a file called `.eslintrc` to the project root:

```js
{
  "extends": "react-app"
}
```

Now your editor should report the linting warnings.

Note that even if you edit your `.eslintrc` file further, these changes will **only affect the editor integration**. They won’t affect the terminal and in-browser lint output. This is because Create React App intentionally provides a minimal set of rules that find common mistakes.

If you want to enforce a coding style for your project, consider using

## Something Missing?

If you have ideas to improve it, [let me know](zeeshan.cornelius@gmail.com) or [contribute some!](https://bitbucket.org/zeeshancornelius/users-planet)
