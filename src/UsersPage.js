import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { setUsers } from './actions/users'
import { getPlanet } from './actions/planet'
import classnames from 'classnames'
import { Header, Modal, Button, Icon } from 'semantic-ui-react'

class UsersPage extends React.Component{

  state = { modalOpen: false, query: "" }

  handleOpen = (e) => this.setState({
    modalOpen: true,
  })

  handleClose = (e) => this.setState({
    modalOpen: false,
  })


  componentDidMount(){
    this.props.setUsers();
  }

  handleFilter = (e) => this.setState({
    query: e.target.value
  })


  getPlanet(link){
    console.log(this.state.users);
    this.props.getPlanet(link);
    this.setState({
      modalOpen: true,
    })
  }

  getNextPrev(link){
    console.log(link);
    this.props.setUsers(link);
  }


  render(){
    const ModalPlanet = (
      <Modal
        trigger={<Button className="hidden" ></Button>}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        basic
        size='small'
      >
        <Header icon='browser' content='Planet Details' />
        <Modal.Content className={this.props.fetchingPlanet?"ui active centered inline loader":""} >
          <table className={classnames('ui', 'green', 'table', {hidden: this.props.fetchingPlanet})} >
            <thead>
              <tr>
                <th>Name</th>
                <th>Diameter</th>
                <th>Climate</th>
                <th>Population</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{this.props.planet.name}</td>
                <td>{this.props.planet.diameter}</td>
                <td>{this.props.planet.climate}</td>
                <td>{this.props.planet.population}</td>
              </tr>
            </tbody>
          </table>
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' onClick={this.handleClose} inverted>
            <Icon name='reply' /> Okay, Go back!
          </Button>
        </Modal.Actions>
      </Modal>
    );
    const emptyMessage=(
      <tr>
        <td colSpan="4" >There are no users yet...</td>
      </tr>
    );

    const rows = this.props.users.map(
      user => <tr key={user.edited} className={user.name.includes(this.state.query) ? "" : user.height.includes(this.state.query) ? "" : user.created.includes(this.state.query) ? "" :"hidden"} >
                <td>{user.name}</td>
                <td>{user.height}</td>
                <td>{user.created}</td>
                <td><a className="point" onClick={this.getPlanet.bind(this ,user.homeworld)} >Planet</a></td>
              </tr>
    );

    const next = (
      <a className="icon item" onClick={this.getNextPrev.bind(this ,this.props.next)}>
        Next<i className="right chevron icon"></i>
      </a>
    );

    const previous = (
      <a className="icon item" onClick={this.getNextPrev.bind(this ,this.props.previous)}>
        <i className="left chevron icon"></i>Previous
      </a>
    );
    return (
      <div className={this.props.fetching?"ui active centered inline loader":""} >
        <div className={this.props.fetching?"hidden":""}>
          <h1>Users List</h1>
          <div className="html ui top attached segment">
            <div className="ui input">
              <input onChange={this.handleFilter} value={this.state.query} type="text" placeholder="Search..." />
            </div>
            {/* {users.users.length === 0 ? emptyMessage : mappedUsers} */}
            <table className="ui celled table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Height</th>
                  <th>Created</th>
                  <th>Planet</th>
                </tr>
              </thead>
              <tbody>
                {this.props.users.length === 0 ? emptyMessage : rows}
              </tbody>
              <tfoot>
                <tr><th colSpan="4">
                    <div className="ui right floated pagination menu">
                      {this.props.previous !== null ? previous : ""}
                      {this.props.next !== null ? next : ""}
                    </div>
                  </th>
                </tr>
              </tfoot>
            </table>
            <div>{ModalPlanet}</div>
          </div>
        </div>
      </div>
    )
  }
}

UsersPage.propTypes={
  users: PropTypes.array.isRequired,
  fetching: PropTypes.bool.isRequired,
  setUsers: PropTypes.func.isRequired,
  getPlanet: PropTypes.func.isRequired,
  fetchingPlanet: PropTypes.bool.isRequired,
  fetchedPlanet: PropTypes.bool.isRequired,
  planet: PropTypes.object.isRequired,
  next: PropTypes.string.isRequired
}

function mapStateToProps(state){
  return {
    users: state.users.users,
    fetching: state.users.fetching,
    planet: state.planet.planet,
    fetchingPlanet: state.planet.fetching,
    fetchedPlanet: state.planet.fetched,
    next: state.users.next,
    previous: state.users.previous
  }
}

export default  connect(mapStateToProps, { setUsers, getPlanet })(UsersPage);
