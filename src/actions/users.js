import axios from "axios";
// export const SET_USERS = 'SET_USERS'

export function setUsers(url="http://swapi.co/api/people/") {
  return function(dispatch) {
    dispatch({type: "FETCHING_USERS"});
    axios.get(url)
      .then((response) => {
        dispatch({type: "FETCH_USERS_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_USERS_REJECTED", payload: err})
      })
  }
}

// export function fetchPlanet(url){
//   return function(dispatch) {
//     dispatch({type: "FETCHING_PLANET"});
//     axios.get(url)
//       .then((response) => {
//         dispatch({type: "FETCH_PLANET_FULFILLED", payload: response.data.results})
//       })
//       .catch((err) => {
//         dispatch({type: "FETCH_PLANET_REJECTED", payload: err})
//       })
//   }
// }


// export function setUsers(users){
//   return{
//     type: SET_USERS,
//     users
//   }
// }
//
// export function fetchUsers(){
//   return dispatch => {
//
//     fetch('http://swapi.co/api/people/')
//       .then(res => res.json())
//       .then( data => dispatch(setUsers(data.results)) )
//
//   }
// }
