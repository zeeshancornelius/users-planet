import axios from "axios";
// export const SET_USERS = 'SET_USERS'

export function getPlanet(url) {
  return function(dispatch) {
    dispatch({type: "FETCHING_PLANET"});
    axios.get(url)
      .then((response) => {
        dispatch({type: "FETCH_PLANET_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_PLANET_REJECTED", payload: err})
      })
  }
}
