import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Route } from 'react-router';
import UsersPage from './UsersPage'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="ui container">
        <div className="ui three item menu" >
          <NavLink className="item" activeClassName="active" exact to="/" >Home</NavLink>
          <NavLink className="item" activeClassName="active" exact to="/users" >Users</NavLink>
          <NavLink className="item" activeClassName="active" exact to="/" >Home</NavLink>
        </div>
          <h1>Welcome to my First React App</h1>
          <Route path="/users" component={UsersPage} />
      </div>
    );
  }
}

export default App;
