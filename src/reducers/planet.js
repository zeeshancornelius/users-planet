

export default function planet(state={
    planet: {},
    fetching: false,
    fetched: false,
    error: null,
  }, action={}){
	switch(action.type){
		case "FETCHING_PLANET":{
			return {...state, fetching: true}
		}
		case "FETCH_PLANET_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }
    case "FETCH_PLANET_FULFILLED": {
      console.log(action.payload);
      return {
        ...state,
        fetching: false,
        fetched: true,
        planet: action.payload
      }
    }
		default: {
        return state;
    }
	}
}

// export default function planet(state={
//     planet: [],
//     fetching: false,
//     fetched: false,
//     error: null,
//   }, action={}){
// 	switch(action.type){
// 		case "FETCHING_PLANET":{
// 			return {...state, fetching: true}
// 		}
// 		case "FETCH_PLANET_REJECTED": {
//         return {...state, fetching: false, error: action.payload}
//       }
//       case "FETCH_PLANET_FULFILLED": {
//         return {
//           ...state,
//           fetching: false,
//           fetched: true,
//           users: action.payload,
//         }
//       }
// 		default: return state;
// 	}
// }
