
export default function users(state={
    users: [],
    fetching: false,
    fetched: false,
    error: null,
    next: "",
    previous: ""
  }, action={}){
	switch(action.type){
		case "FETCHING_USERS":{
			return {...state, fetching: true}
		}
		case "FETCH_USERS_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }
      case "FETCH_USERS_FULFILLED": {
        console.log(action.payload);
        return {
          ...state,
          fetching: false,
          fetched: true,
          users: action.payload.results,
          next: action.payload.next,
          previous: action.payload.previous
        }
      }
		default: return state;
	}
}
