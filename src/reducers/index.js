import { combineReducers } from "redux"

import users  from "./users"
import planet from "./planet"

export default combineReducers({
  users,
  planet
});
